<?php
/* Подключение к серверу MySQL */
$link = mysqli_connect(
    'std-mysql', /* Хост, к которому мы подключаемся */
    'std_228', /* Имя пользователя */
    'RMN013mr', /* Используемый пароль */
    'std_228'); /* База данных для запросов по умолчанию */
if (!$link) { printf("Невозможно подключиться к базе данных. Код ошибки: %s\n", mysqli_connect_error()); exit; }

$result = mysqli_query($link, 'SELECT * FROM `coursework_category`');
//$row = mysqli_fetch_array($result)

$category = $_GET['category'];

?>
<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="styles.css">
    <title>T-Store | Интернет-магазин электроники</title>
</head>
<body>
<header>
    <a href="index.php"><div class="logo"><img src="imgs/logo.png" alt=""></div></a>
    <a href="cart.php" class="basket"><img src="imgs/cart.png" alt=""><div class="basket-cart"><div>Корзина</div><div id="sumAndCount"></div></div></a>
</header>

<main>
    <nav>
        <div><a href='index.php'>Главная</a></div>
        <?php
        while($row = mysqli_fetch_array($result)){
            echo "<div><a href='items.php?category=".$row['category']."'>".$row['name']."</a></div>";
        }

        ?>
        <div><a href='cart.php'>Корзина</a></div>
        <div><a href='contacts.php'>Контакты</a></div>
    </nav>

    <div class="content">
        <?php
        $result = mysqli_query($link, 'SELECT * FROM `coursework_category` WHERE `category`='."'".$category."'");
        while($row = mysqli_fetch_array($result)){
            echo "<div class='items-header' style='background-color:".$row['bg-color']."'>"."#".$row['name']."</div>";
        }
        ?>
        <div class="items-container">
            <?php
                $result = mysqli_query($link, 'SELECT * FROM `coursework_items` WHERE `category`='."'".$category."'");
                while($row = mysqli_fetch_array($result)){
                    $price = json_decode($row['modifiers']);
                    echo "<a href='item.php?item=".$row['id']."'>";
                    echo "<div class='item'>";
                    echo "<img src='".$row['img']."' alt ='item'>";
                    echo $row['name'];
                    echo "<div class='price-buy'>";
                    echo "<div>От ".$price->{'price'}."</div>" ;
                    echo "<div class='item-buy'>Посмотреть</div>";
                    echo "</div>";
                    echo "</div>";
                    echo "</a>";
                }

            ?>
        </div>
    </div>
</main>

<footer>2019 | @yak0v13v</footer>
</body>
<script>
    let sumAndCount = () =>{
        let sum = 0;
        let count = 0;
        var elements = localStorage.getItem("arr").split(", ");
        for(let i=0; i<elements.length;i++){
            let obj = JSON.parse(elements[i]);
            sum += obj["price"]*obj['count'];
            count += obj['count'];
        }
        document.getElementById("sumAndCount").innerHTML = `${count}/${sum.toLocaleString()} руб`;
    };
    sumAndCount();
</script>
</html>