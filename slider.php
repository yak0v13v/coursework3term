<?php
/* Подключение к серверу MySQL */
$link = mysqli_connect(
    'std-mysql', /* Хост, к которому мы подключаемся */
    'std_228', /* Имя пользователя */
    'RMN013mr', /* Используемый пароль */
    'std_228'); /* База данных для запросов по умолчанию */
if (!$link) { printf("Невозможно подключиться к базе данных. Код ошибки: %s\n", mysqli_connect_error()); exit; }

$result = mysqli_query($link, 'SELECT * FROM `coursework_slider`');
$bool = true;
while($row = mysqli_fetch_array($result)){
    if($bool == true){
        $img1 = $row['img'];
        $bool = false;
    }else{
        $img2 = $row['img'];
    }
}

?>
<div class="container_slider">

    <input type="radio" id="i1" name="images" checked>
    <input type="radio" id="i2" name="images">


    <div class="slide_img" id="one">
        <img src="<?php echo $img1; ?>">
        <label class="prev" for="i2"><span></span></label>
        <label class="next" for="i2"><span></span></label>
    </div>

    <div class="slide_img" id="two">
        <img src="<?php echo $img2; ?>">
        <label class="prev" for="i1"><span></span></label>
        <label class="next" for="i1"><span></span></label>
    </div>


    <div id="nav_slide">
        <label for="i1" class="dots" id="dot1"></label>
        <label for="i2" class="dots" id="dot2"></label>

    </div>

</div>