<?php
/* Подключение к серверу MySQL */
$link = mysqli_connect(
    'std-mysql', /* Хост, к которому мы подключаемся */
    'std_228', /* Имя пользователя */
    'RMN013mr', /* Используемый пароль */
    'std_228'); /* База данных для запросов по умолчанию */
if (!$link) { printf("Невозможно подключиться к базе данных. Код ошибки: %s\n", mysqli_connect_error()); exit; }

$result = mysqli_query($link, 'SELECT * FROM `coursework_category`');
//$row = mysqli_fetch_array($result)

$item = $_GET['item'];
$category = 0;
$item_name;
$description;
$img;
$modifiers;
$price = 0;
$result1 = mysqli_query($link, 'SELECT * FROM `coursework_items` WHERE `id`='."'".$item."'");
while($row1 = mysqli_fetch_array($result1)){
    $category = $row1['category'];
    $item_name = $row1['name'];
    $description = $row1['description'];
    $img = $row1['img'];
    $modifiers = json_decode($row1['modifiers']);
}
?>
<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="styles.css">
    <title>T-Store | Интернет-магазин электроники</title>
</head>
<body>
<header>
    <a href="index.php"><div class="logo"><img src="imgs/logo.png" alt=""></div></a>
    <a href="cart.php" class="basket"><img src="imgs/cart.png" alt=""><div class="basket-cart"><div>Корзина</div><div id="sumAndCount"></div></div></a>
</header>

<main>
    <nav>
        <div><a href='index.php'>Главная</a></div>
        <?php
        while($row = mysqli_fetch_array($result)){
            echo "<div><a href='items.php?category=".$row['category']."'>".$row['name']."</a></div>";
        }

        ?>
        <div><a href='cart.php'>Корзина</a></div>
        <div><a href='contacts.php'>Контакты</a></div>
    </nav>

    <div class="content">
        <?php
        $result = mysqli_query($link, 'SELECT * FROM `coursework_category` WHERE `category`='."'".$category."'");
        while($row = mysqli_fetch_array($result)){
            echo "<div class='items-header' style='background-color:".$row['bg-color']."'>"."#".$row['name']."  #".$item_name."</div>";
        }
        ?>
        <div class="item-content">
            <div class="item-img-mod">
                <img src="<?php echo $img; ?>" alt="Img">
                <div>
                    <div class="item-name-desk">
                    <h1><?php echo $item_name; ?></h1>
                    <div>
                        <?php
                            //echo $modifiers -> {"modifiers"}[0] ->{"name"};
                            //echo "<b>".var_dump($modifiers)."</b>";
                        for($i = 0; $i < count($modifiers -> {"modifiers"}); $i++){
                            echo "<div>".$modifiers->{"modifiers"}[$i] -> {"name"}.":</div>";
                            echo "<div>";
                            for($j =0; $j < count($modifiers->{"modifiers"}[$i] -> {"mod"}); $j++){
                                if($j == 0){
                                    echo "<label>";
                                    echo '<input type="radio" name="'.$modifiers->{"modifiers"}[$i] -> {"name"}.'" value="'.$modifiers->{"modifiers"}[$i] -> {"mod"}[$j] -> {"plusPrice"}.'" checked onchange="change(this.value, this.id)" data-first="'.$modifiers->{"modifiers"}[$i] -> {"name"}.'" id="'.$i.$j.'">';
                                    echo $modifiers->{"modifiers"}[$i] -> {"mod"}[$j] -> {"name"}."</label>";
                                    $price += $modifiers->{"modifiers"}[$i] -> {"mod"}[$j] -> {"plusPrice"};
                                }else{
                                    echo "<label>";
                                    echo '<input type="radio" name="'.$modifiers->{"modifiers"}[$i] -> {"name"}.'" value="'.$modifiers->{"modifiers"}[$i] -> {"mod"}[$j] -> {"plusPrice"}.'" onchange="change(this.value, this.id)" data-first="'.$modifiers->{"modifiers"}[$i] -> {"name"}.'" id="'.$i.$j.'">';
                                    echo $modifiers->{"modifiers"}[$i] -> {"mod"}[$j] -> {"name"}."</label>";
                                }

                            }
                            echo "</div>";
                        }
                        ?></div>
                </div>
                <div>
                    <span class="price" id="price"><?php echo $modifiers->{"price"}+$price;?></span>
                    <span class="btn-buy" onclick="buy()">Купить</span>
                </div>
            </div>

            </div>
            <div class="item-desk"><?php echo $description; ?></div>

        </div>

    </div>
</main>

<footer>2019 | @yak0v13v</footer>
<script>
    var first = [<?php for($k = 0; $k < count($modifiers -> {"modifiers"}); $k++){
            echo $modifiers -> {"modifiers"}[$k] -> {"mod"}[0] -> {"plusPrice"}.",";
    } ?>];

    var change = (value, id) =>{
        let cost = document.getElementById('price');
        cost.innerHTML = Number(cost.innerHTML) - Number(first[id[0]]) + Number(value);
        first[id[0]] = value;
    };
    var buy = () => {
        let cost = Number(document.getElementById('price').innerHTML);
        var obj = {
            item: "<?php echo $item_name; ?>",
            price: cost,
            count: 1,
            mods: []
        };
        let perebor = document.getElementsByTagName("input");
        for(let i=0; i< perebor.length; i++){
            if(perebor[i].checked === true){
                obj["mods"].push(perebor[i].parentElement.innerText);
            }
        }
        console.log(obj);

        if(localStorage.getItem("arr") == null){
            console.log(localStorage.getItem("arr"));
            localStorage.setItem("arr", JSON.stringify(obj));
        }else{
            localStorage.setItem("arr", localStorage.getItem("arr")+", "+JSON.stringify(obj));
        }
        sumAndCount();

    };

    let sumAndCount = () =>{
        let sum = 0;
        let count = 0;
        var elements = localStorage.getItem("arr").split(", ");
        for(let i=0; i<elements.length;i++){
            let obj = JSON.parse(elements[i]);
            sum += obj["price"]*obj['count'];
            count += obj['count'];
        }
        document.getElementById("sumAndCount").innerHTML = `${count}/${sum.toLocaleString()} руб`;
    };
    sumAndCount();
</script>
</body>
</html>