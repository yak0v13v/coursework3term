<?php
/* Подключение к серверу MySQL */
$link = mysqli_connect(
    'std-mysql', /* Хост, к которому мы подключаемся */
    'std_228', /* Имя пользователя */
    'RMN013mr', /* Используемый пароль */
    'std_228'); /* База данных для запросов по умолчанию */
if (!$link) { printf("Невозможно подключиться к базе данных. Код ошибки: %s\n", mysqli_connect_error()); exit; }

$result = mysqli_query($link, 'SELECT * FROM `coursework_category`');
//$row = mysqli_fetch_array($result)

?>
<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="styles.css">
    <title>Корзина | T-Store</title>
</head>
<body>
<header>
    <a href="index.php"><div class="logo"><img src="imgs/logo.png" alt=""></div></a>
    <a href="cart.php" class="basket"><img src="imgs/cart.png" alt=""><div class="basket-cart"><div>Корзина</div><div id="sumAndCount"></div></div></a>
</header>

<main>
    <nav>
        <div><a href='index.php'>Главная</a></div>
        <?php
        while($row = mysqli_fetch_array($result)){
            echo "<div><a href='items.php?category=".$row['category']."'>".$row['name']."</a></div>";
        }

        ?>
        <div><a href='cart.php'>Корзина</a></div>
        <div><a href='contacts.php'>Контакты</a></div>
    </nav>

    <div class="content">
        <div class="cart-header">
            <span class="hed-cart">Корзина</span>
            <span class="clr-cart" onclick="clearBasket()">Очистить корзину</span>
        </div>
        <div class="cart-container">
            <script>
                if(localStorage.getItem("arr") === null){
                    document.write("<h2 align='center'>Корзина пуста</h2>")
                }else{
                    var elements = localStorage.getItem("arr").split(", ");
                    for(let i=0; i<elements.length;i++){
                        let obj = JSON.parse(elements[i]);
                        document.write(`<div class="cart-items-container" id='${i}'><div>${obj["item"]}</div><div>${obj["mods"].map((item)=>" "+item)}</div><div class="price">${obj["price"]}</div><div><span class="minuse" onclick="changeCount('-', ${i})">-</span><span class="count">1</span><span class="pluse" onclick="changeCount('+',${i})">+</span></div><div class="itogo">${obj['price']}</div><div class="delete" onclick="deleteItem(${i})">X</div></div>`)
                    }
                }
            </script>
        </div>
        <div id="sum">
            <script>
                let cost = 0;
                let itogo = document.getElementsByClassName("itogo");
                for(let i =0; i<itogo.length;i++){
                    cost += Number(itogo[i].innerHTML);
                }
                if(cost !== 0)
                document.write(cost);
                document.getElementById('data').value = localStorage.getItem("arr");
            </script>
        </div>
        <div class="doOrder">
            <form action="processOrder.php" method="post">
                <label>ФИО:<input type="text" placeholder="Иванов Иван Иванович" name="name"></label>
                <label>Номер телефона:<input type="text" placeholder="+7(900) 000 00 00" name="phone"></label>
                <label>Email:<input type="email" placeholder="example@t-store.ru" name="email"></label>
                <input type="text" value="" style="display: none;" id="data" name="data">
                <input type="submit" value="Заказать" id="submit">
            </form>
        </div>
    </div>
</main>

<footer>2019 | @yak0v13v</footer>

<script>
    document.getElementById('data').value = localStorage.getItem("arr");
    function onOff(){
        if(localStorage.getItem("arr")===undefined){
            document.getElementById("submit").disabled=true;
        }else{
            document.getElementById("submit").disabled=false;
        }
    }
    onOff();

    let clearBasket = ()=>{
        localStorage.clear();
        location.reload();
    }

    let changeCount = (param, id) =>{
        let plus = document.getElementById(id);
        if(param == "+"){
           Number(plus.getElementsByClassName("count")[0].innerHTML++);
            plus.getElementsByClassName("itogo")[0].innerHTML = Number(plus.getElementsByClassName("price")[0].innerHTML)*Number(plus.getElementsByClassName("count")[0].innerHTML);
        }else if(param=="-"){
            Number(plus.getElementsByClassName("count")[0].innerHTML--);
            plus.getElementsByClassName("itogo")[0].innerHTML = Number(plus.getElementsByClassName("price")[0].innerHTML)*Number(plus.getElementsByClassName("count")[0].innerHTML);
        }
        computingPrice();
    };

    let deleteItem = (id) => {
        let div = document.getElementById(id);
        div.parentNode.removeChild(div);
        var elements = localStorage.getItem("arr").split(", ");
        localStorage.clear();
        elements.splice(id,1);
        localStorage.setItem("arr", elements.join(", "));
        computingPrice();
    };

    let computingPrice = () =>{
        let cost = 0;
        let itogo = document.getElementsByClassName("itogo");
        for(let i =0; i<itogo.length;i++){
            cost += Number(itogo[i].innerHTML);
        }
        document.getElementById('sum').innerHTML = cost;
        document.getElementById('data').value = localStorage.getItem("arr");
        onOff();
    };
    let sumAndCount = () =>{
        let sum = 0;
        let count = 0;
        var elements = localStorage.getItem("arr").split(", ");
        for(let i=0; i<elements.length;i++){
            let obj = JSON.parse(elements[i]);
            sum += obj["price"]*obj['count'];
            count += obj['count'];
        }
        document.getElementById("sumAndCount").innerHTML = `${count}/${sum.toLocaleString()} руб`;
    };
    sumAndCount();
</script>
</body>
</html>